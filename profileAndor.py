import atst.base.util.Misc as BaseMisc
import atst.base.util.RemoteAction as RemoteAction
import atst.base.util.ra.NullAction as NullAction
import atst.cs.controller.ActionResponse as ActionResponse
import atst.cs.data.AttributeTable as AttributeTable
import atst.cs.data.Configuration as Configuration
import atst.cs.services.Event as Event
import atst.cs.services.Log as Log
import atst.cs.services.event.EventCallbackAdapter as EventCallbackAdapter
import atst.cs.test.TestConfiguration as TestConfiguration
import atst.css.util.csh.parts.Sampling as Sampling
import atst.css.util.csh.parts.Shutter as Shutter
import atst.css.util.CameraId as CameraId
import atst.css.util.DATComponentException as DATComponentException
import atst.css.util.VCAttributeSets as VCAttribute
import atst.css.util.camconfig.CamConfig as CamConfig
import atst.css.util.camconfig.CamConfigFactory as CamConfigFactory
import atst.css.util.camconfig.parts.Accum as Accum
import atst.css.util.camconfig.parts.Calculated as Calculated
import atst.css.util.camconfig.parts.CamConfigId as CamConfigId
import atst.css.util.camconfig.parts.Exposure as Exposure
import atst.css.util.camconfig.parts.Normalization as Normalization
import atst.css.util.camconfig.parts.WinBin as WinBin
import atst.css.util.camconfig.parts.WinBinHw as WinBinHw
import atst.css.util.camconfig.parts.WinBinSw as WinBinSw
import atst.css.util.camconfig.parts.Roi as Roi
import atst.css.util.camconfig.parts.XYPair as XYPair

from functools import wraps
import argparse
import copy
import csv
import string
import sys
import time
from time import sleep
from datetime import datetime, timedelta

PROFILE_BIN='bin'
PROFILE_BIN_EXP='bin_exp'
PROFILE_EXP='exp'
PROFILE_ROI='roi'
PROFILE_ROI_EXP='roi_exp'
#PROFILE_ROIBIN='roiBin'
#PROFILE_ROIbin_exp='roibin_exp'
PROFILING_CHOICES=[PROFILE_BIN, PROFILE_BIN_EXP, PROFILE_EXP,\
                   PROFILE_ROI, PROFILE_ROI_EXP] #, PROFILE_ROIBIN, PROFILE_ROIbin_exp]

SHUTTER_GLOBAL='Global'
SHUTTER_ROLLING='Rolling'
SHUTTER_MODES=[SHUTTER_GLOBAL, SHUTTER_ROLLING]

Bins = []
binSizes = [[1,1], [2,2], [1,1], [3,3], [1,1], [4,4], [1,1], [8,8],
            [2,2], [3,3], [2,2], [4,4], [2,2], [8,8],
            [3,3], [4,4], [3,3], [8,8],
            [4,4], [8,8],
            [1,1]]
for size in binSizes:
    Bins.append(XYPair.create(size))


NUM = 0           # Index to Range Number
MIN = 1           # Index to Minimum Exposure Time of an Exposure Range
MAX = 2           # Index to Maximum Exposure Time of an Exposure Range
INC = 3           # Index to Exposure Time Increment of an Exposure Range
EXP_RANGE_1 = [1,  0.0,  0.1, 0.0001]
EXP_RANGE_2 = [2,  0.1,  1.0, 0.001]
EXP_RANGE_3 = [3,  1.0, 10.0, 0.01]
EXP_RANGE_4 = [4, 10.0, 30.0, 0.1]
EXP_RANGE_5 = [5, 30.0,  0.0, 1.0]
EXP_RANGES = [EXP_RANGE_1, EXP_RANGE_2, EXP_RANGE_3, EXP_RANGE_4, EXP_RANGE_5]

DEF_EXP_RATE = 0.2    # Hz
DEF_EXP_TIME = 20.0   # milli-seconds
PROF_EXP_RATE = 20.0  # Hz
PROF_EXP_TIME = 5.0   # milli-seconds

EXP_TYPE_SHORT = 'short'
EXP_TYPE_LONG  = 'long'
EXP_TYPE_PINGPONG = 'pingPong'

# Exposure types accepted by --expType
EXP_TYPES = [EXP_TYPE_SHORT, EXP_TYPE_LONG, EXP_TYPE_PINGPONG]

EXP_TYPE_IDX = {EXP_TYPE_SHORT: 0, EXP_TYPE_LONG: 1, EXP_TYPE_PINGPONG: 2}
EXP_TIME_INC = {EXP_TYPE_SHORT: 0.0001, EXP_TYPE_LONG: 0.001, EXP_TYPE_PINGPONG: 0.001}

ROI_ORIGIN_X = 'originX'
ROI_ORIGIN_Y = 'originY'
ROI_SIZE_X   = 'sizeX'
ROI_SIZE_Y   = 'sizeY'
ROI_PARAMS = [ROI_ORIGIN_X, ROI_ORIGIN_Y, ROI_SIZE_X, ROI_SIZE_Y]
DEF_ROI_INC = 1
MIN_ROI_INC = 1

RESULTS_TEST = 'Test'
RESULTS_BINX = 'BinX'
RESULTS_BINY = 'BinY'
RESULTS_ORIGX = 'origX'
RESULTS_ORIGY = 'origY'
RESULTS_SIZEX = 'sizeX'
RESULTS_SIZEY  = 'sizeY'
RESULTS_STOP = 'Stop'
RESULTS_EXPTIME = 'expTime'
RESULTS_CFGEXP = 'cfgExp'
RESULTS_CFGWINBIN = 'cfgWinBin'
RESULTS_START = 'Start'
RESULTS_TOTAL = 'Total'
RESULTS_CCID  = 'ccId'
RESULTS_KEYS = [RESULTS_TEST, RESULTS_BINX, RESULTS_BINY, RESULTS_ORIGX, RESULTS_ORIGY, 
                RESULTS_SIZEX, RESULTS_SIZEY, RESULTS_EXPTIME, RESULTS_STOP,
                RESULTS_CFGEXP, RESULTS_CFGWINBIN, RESULTS_START, 
                RESULTS_TOTAL, RESULTS_CCID]

#results = {field_name: [] for field_name in RESULTS_KEYS}
#results = {(list(string.ascii_lowercase)[l] + field_name): [] for l, field_name in enumerate(RESULTS_KEYS, 0)}

# create a dictionary of fieldnames:int so that we can later sort the results
# and have then come out in the same order as the RESULTS_KEYS
# We can get the mapped int value using the field name as a key
results_key_xref = {key: i for i, key in enumerate(RESULTS_KEYS,1)}

# Create a dictionary for the results: <int>:[] 
# <int> maps to a field name in field_key_xref
results = {i: [] for i in range(1, len(results_key_xref) + 1)}


# Test counters
event_counter = 0
dryRun_counter = 0


# Custom action that can be used by the argument parser to check whether a
# supplied value is >= 0.
# Note that geq 0 is equivalent to using HasLimitsAction where
# minium=0, maximum=None. This is just clearer (I think).
class GEQZeroAction(argparse.Action):
    def __call__(self, parser, namespace, value, option_string=None):
        if not value >= 0:
            raise argparse.ArgumentError(self, 'time value must be >= 0')
        setattr(namespace, self.dest, value)


# Custom action that can be used by the argument parser to check whether a 
# supplied value is in defined bounds:
#  - If minimum != None and maximum != None then check: minimum <= value <= maximum
#  - If minimum == None then check: value <= maximum
#  - If maximum == None then check: value >= minimum
# Note that checks are separated to allow for custom error response based on 
# whether this custom action is being used with a defined minimum, maximum or both.
class HasLimitsAction(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, const=None, default=None,\
                 type=None, choices=None, required=False, help=None, metavar=None,\
                 minimum=None, maximum=None):
        self.min = minimum
        self.max = maximum
        super(HasLimitsAction, self).__init__(\
            option_strings, dest, nargs, const, default,\
            type, choices, required, help, metavar)

    def __call__(self, parser, namespace, values, option_string=None):
        if not self.min is None and not self.max is None:
            if not values >= self.min or not values <= self.max:
                raise argparse.ArgumentError(self,
                    'value must be in the range {} <= value <= {}'.format(self.min,self.max))
        elif self.min is None and not self.max is None:
            if not values <= self.max:
                raise argparse.ArgumentError(self, 
                    'value must be <= {}'.format(self.max))
        elif not self.min is None and self.max is None:
            if not values >= self.min:
                raise argparse.ArgumentError(self, 
                    'value must be >= {}'.format(self.min))
        setattr(namespace, self.dest, values)


# Custom action that can be used by the argument parser to add an integer
# argument >= 0 to a dictionary. Use to process arguments of the form:
#     --arg KEY=VALUE [KEY=VALUE]...
class ParseIntDictGEQZeroAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        d = getattr(namespace, self.dest, {})

        if values:
            for item in values:
                split_items = item.split('=', 1)      # split key and value
                key = split_items[0].strip()          # keys are upper case
                value = int(split_items[1])           # convert to integer

                # Is this a valid key for the current argument?
                if not key in keys.CMDLINE_ARGS[self.dest]:
                    raise argparse.ArgumentError(self, 
                        'invalid key for argument, valid keys are {}'.format(
                            keys.CMDLINE_ARGS[self.dest]))

                # Is value geq 0?
                if not value >= 0:
                    raise argparse.ArgumentError(self, 
                        'time value must be >= 0, {}={}'.format(key, value))

                d[key] = value
        setattr(namespace, self.dest, d)

# Setup parser for command-line arguments
cmdLineParser = argparse.ArgumentParser(add_help=True)
cmdLineParser.add_argument('--debug', action='store_true',
    help='Enable debug output.')

cmdLineParser.add_argument('--noprompt', action='store_true',
    help='HACK.')

cmdLineParser.add_argument('--dryRun', action='store_true',
    help='Execute without submitting configurations to CSS.')

cmdLineParser.add_argument('--verbose', action='store_true',
    help='increase output verbosity')

# Positional arguments
cmdLineParser.add_argument('vccName', action='store', 
    help='Name of CSS Management Controller.')

# Optional arguments
cmdLineParser.add_argument('--delete_all_ids', action='store_true',
    help='Clean all loaded IDs on completion.')

cmdLineParser.add_argument('--gainMode', action='store', dest='gainMode',
    help='Specifies a specific gain mode to use.')

cmdLineParser.add_argument('--monitorOnly', action='store_true', dest='monitorOnly',
    help='Passively monitor a running instance of the CSS and print profiling data.'\
         'This flag is mutually exclusive with all other options!')

cmdLineParser.add_argument('--shutterMode', choices=SHUTTER_MODES,
    nargs='+', action='store', dest='shutterModes',
    default=SHUTTER_MODES,
    help='Shutter modes to profile.')
    
cmdLineParser.add_argument('--profile', choices=PROFILING_CHOICES,
    nargs='+', action='store', dest='paramsToProfile',
    default=PROFILING_CHOICES,
    help='CamConfig changes to profile.')

cmdLineParser.add_argument('--expType', choices=EXP_TYPES,
    nargs='+', action='store', dest='exposureTypes',
    default=EXP_TYPES,
    help='Types of exposure times: \'pingPong\' across Long Exposure Transition (LET)'\
         ', \'short\'(exposure times < LET), \'long\'(exposure times >= LET).')

cmdLineParser.add_argument('--roiParams', choices=ROI_PARAMS,
    nargs='+', action='store', dest='roiParams',
    default=ROI_PARAMS,
    help='Types of ROI tests: Vary originX, originY, sizeY, sizeY, between min '\
         'and max, one at a time. NOTE: For any given roiParam being tested, '\
         'other parameters may also change in order to keep the ROI size within '\
         'the defined limits of the camera sensor.')

cmdLineParser.add_argument('--roiInc', type=int,
    action=HasLimitsAction, dest='roiInc', default=DEF_ROI_INC,
    minimum=MIN_ROI_INC,     # <-- Custom arg for custom action
    help='Specifies the amount to increment the roi parameter under test.')


EXECUTION_TIME_DATA = {}

def ExecutionTime(fn):
    @wraps(fn)
    def with_execution_time(*args, **kwargs):
        start_time = time.time()

        ret = fn(*args, **kwargs)

        elapsed_time = time.time() - start_time


        if fn.__name__ not in EXECUTION_TIME_DATA:
            EXECUTION_TIME_DATA[fn.__name__] = [[], []]
        if fn.__name__ == 'passively_monitor':
            EXECUTION_TIME_DATA[fn.__name__][0].append(None)
        else:
            EXECUTION_TIME_DATA[fn.__name__][0].append(args[1])
        EXECUTION_TIME_DATA[fn.__name__][1].append(elapsed_time)

        return ret

    return with_execution_time

def print_execution_time():
    totalExecutionTime = 0.0
    for fname, data in EXECUTION_TIME_DATA.items():
        for i in range(0, len(data[0])):
            # skip individual test paramteter summary for passive monitoring
            # and only print the total execution time.
            if not fname == 'passively_monitor':
                print('Profile: {},'.format(fname)),
                print('Shutter Mode: {},'.format(data[0][i])),
                print('Duration: {:.3f}sec'.format(data[1][i]))
            totalExecutionTime += data[1][i]
    print('Total Execution Time: {}'.format(timedelta(seconds=totalExecutionTime)))

def clear_execution_time_data():
    global EXECUTION_TIME_DATA
    EXECUTION_TIME_DATA = {}


# Print current profile data to screen in a nice format...
def print_profile_data(currData):
    print('{:>5d},{:>5d},{:>5d},{:^8d},{:^8d},{:^8d},{:^8d}, {:^11.06f}, {:.06f}, {:.06f}, {:.06f}, {:.06f}, {:.06f}, {}'.format(
        currData[RESULTS_TEST],
        currData[RESULTS_BINX], currData[RESULTS_BINY],
        currData[RESULTS_ORIGX], currData[RESULTS_ORIGY],
        currData[RESULTS_SIZEX], currData[RESULTS_SIZEY],
        currData[RESULTS_EXPTIME], 
        currData[RESULTS_STOP], currData[RESULTS_CFGEXP], 
        currData[RESULTS_CFGWINBIN], currData[RESULTS_START],
        currData[RESULTS_TOTAL], currData[RESULTS_CCID]) )


class TimingEventListener(EventCallbackAdapter):
    def callback(self, eventName):
        global results
        global results_key_xref
        global event_counter
        global print_profile_data

        event_counter += 1
        binSize = XYPair.create(self.getIntegerArray('binSize'))
        roi = Roi.create(self.getIntegerArray('roi'))
        expTime = self.getDouble('expTime')
        stopAcq = self.getDouble('stopAcq')
        cfgExp = self.getDouble('cfgExposure')
        cfgWinBin = self.getDouble('cfgWinBin')
        startAcq = self.getDouble('startAcq')
        total = self.getDouble('total')
        ccId = self.getString('ccId')

        results[results_key_xref[RESULTS_TEST]].append(event_counter)
        results[results_key_xref[RESULTS_BINX]].append(binSize.x()) 
        results[results_key_xref[RESULTS_BINY]].append(binSize.y()) 
        results[results_key_xref[RESULTS_ORIGX]].append(roi.origin().x()) 
        results[results_key_xref[RESULTS_ORIGY]].append(roi.origin().y())
        results[results_key_xref[RESULTS_SIZEX]].append(roi.size().x())
        results[results_key_xref[RESULTS_SIZEY]].append(roi.size().y()) 
        results[results_key_xref[RESULTS_EXPTIME]].append(expTime)
        results[results_key_xref[RESULTS_STOP]].append(stopAcq)
        results[results_key_xref[RESULTS_CFGEXP]].append(cfgExp)
        results[results_key_xref[RESULTS_CFGWINBIN]].append(cfgWinBin)
        results[results_key_xref[RESULTS_START]].append(startAcq)
        results[results_key_xref[RESULTS_TOTAL]].append(total)
        results[results_key_xref[RESULTS_CCID]].append(ccId)

        # Make a dict with the results key and the data just added and pass that 
        # to the print method for screen output
        currData = {key: results[results_key_xref[key]][len(results[results_key_xref[key]])-1] for key in RESULTS_KEYS}
        print_profile_data( currData )


class Profile:
    error = False

    # Class variables
    # Default constructor
    def __init__(self,cmdLineArgs):
        self.error = False
        self._clArgs = cmdLineArgs     # copy command line arguments

        self._vccName = cmdLineArgs.vccName
        self._vccCamName = cmdLineArgs.vccName + '.cam'
        self._timingInfoEventName = self._vccCamName + '.ccTimingInfo'
        self._vcc = App.connect(self._vccName)
        self._vccCam = App.connect(self._vccCamName)
        self._current_profile_test_name = None

        self.listener = TimingEventListener()   # CamConfig Timing Info

        at = AttributeTable()
        at.setFQPrefix(self._vccName)
        at = at.requalify(self._vccName)
        at.insert('.global:ID0')
        at.insert('.global:triggerReference')
        at.insert('.global:cameraID')
        at.insert('.csh:shutter:mode')
        at = self._vcc.get(at)
        at.setFQPrefix(self._vccName)

        # Create CamConfigFactory and then create an temp camConfig well use
        # to retrieve 'cc_default' from the CSS.
        tmpCC = None
        try:
            self._ccFactory = CamConfigFactory( \
                CameraId.create(at.getStringArray('.global:cameraID')) )

            # Use cc_default for the ID. We'll need an attribute table with
            # that ID later to retrieve cc_default from the CSS.
            ccId = CamConfigId.create('cc_default')
            ccDescription = 'Temporary CamConfig.'
            tmpCC = self._ccFactory.create( ccId, ccDescription, DEF_EXP_RATE, DEF_EXP_TIME)

            self._roi_fullFrame = tmpCC.winBin().hw().ROI_1()
            self._sensorSizeX = self._roi_fullFrame.size().x()
            self._sensorSizeY = self._roi_fullFrame.size().y()
            self._maxExposureTime = tmpCC.calculated().maximumExposureTime() / 1000.0
            self._minExposureTime = tmpCC.calculated().minimumExposureTime() / 1000.0
        except DATComponentException as ex:
            print(ex.getMessage())
            exit

        # We'll need the RemotePropertyHelper and we need a full-frame ROI
        # definition and the sensor dimeensions
        self._rph = self._ccFactory.getRemotePropertyHelper()

        # Get the attribute table for the tmp CC we created above. Leave in the ID
        # because we want to get the 'cc_default' camConfig from the CSS.
        # Remove all camConfig;calcualted namespace attributes. 
        at = tmpCC.toAttributeTable().requalify(self._vccName)
        at.setFQPrefix(self._vccName)
        at.extractOnPrefix(Calculated.getNamespace())

        # Get CC default attributes, create a CamConfig from them and save it.
        at = self._vcc.get(at)
        at.setFQPrefix(self._vccName)
        self._ccDefault = self._ccFactory.create(at)
        self._refCC = None            # Established when shutter mode is set

        # List of initial exposure times per exposure type 
        self._initExpTime = []        # Configured with init_exposure_times()
        self._nextExpTime  = []        # Current exposure time for test
        self._currExpTime = []

        # ROI related values that may be needed.
        self._cshWinParamsX = self._rph.getIntegers(VCAttribute.CSH_WIN_PARAMS_X)
        self._cshWinParamsY = self._rph.getIntegers(VCAttribute.CSH_WIN_PARAMS_Y)
        self._roiParamMin = 0           # Min value for roi param under test
        self._roiParamMax = 0           # Max value for roi Param under test


    # Initialize the exposure time dictionaries according to the current 
    # reference camera configuration which was established when the shutter mode
    # was set.
    def init_exposure_times(self):
        global EXP_TIME_INC

        # Establish initial short/long/pingPong exposure times (in sec)
        # Note: Rolling Shutter doesn't have a LongExposureTransition. 
        #       Set with a nominal value
        if self._ccFactory.getShutter().mode() is Shutter.Mode.Global:
            self._initExpTime =\
                {EXP_TYPE_SHORT:    self._refCC.calculated().minimumExposureTime() / 1000.0,
                 EXP_TYPE_LONG:     self._refCC.calculated().longExposureTransition(),
                 EXP_TYPE_PINGPONG: self._refCC.calculated().longExposureTransition()}
        else:
            self._initExpTime =\
                {EXP_TYPE_SHORT:    self._refCC.calculated().minimumExposureTime() / 1000.0,
                 EXP_TYPE_LONG:     0.014,
                 EXP_TYPE_PINGPONG: 0.014}


        # Initialize next exposure times to initial value.
        self._nextExpTime = \
            {EXP_TYPE_SHORT: self._initExpTime[EXP_TYPE_SHORT],
             EXP_TYPE_LONG:  self._initExpTime[EXP_TYPE_LONG]}
        self._currPingPongType = EXP_TYPE_LONG
        return

    def create_exposure(self, type, currentLET):
        global EXP_TIME_INC

        localType = type
        if type == EXP_TYPE_PINGPONG:
            if self._currPingPongType == EXP_TYPE_LONG: 
                self._currPingPongType = EXP_TYPE_SHORT
            else:
                self._currPingPongType = EXP_TYPE_LONG
            localType = self._currPingPongType

        # Create CSS Exposure object...
        exposure = Exposure.create( \
            1.0/(self._nextExpTime[localType]+1.0),    # Assure compatible rate
            self._nextExpTime[localType] * 1000.0)     # Converted to milliseconds

        # Increment exposure time and prepare for next test...
        nextExpTime = self._nextExpTime[localType] + EXP_TIME_INC[localType]

        # Look for new expsoure time crossing a threshold and reset if needed...
        # Yes, this could be reduced. Leaving it this way for clarity.
        shutterMode = self._ccFactory.getShutter().mode()
        if (shutterMode is Shutter.Mode.Global and
            ((localType == EXP_TYPE_SHORT and nextExpTime >= currentLET) or
             (localType == EXP_TYPE_LONG  and nextExpTime > self._maxExposureTime))) or \
           (shutterMode is Shutter.Mode.Rolling and nextExpTime > self._maxExposureTime):
            # New exposure time crossed some threshold. Reset to initial 
            # exposure time for type and start from there on next iteration.
            nextExpTime = self._initExpTime[type]

        # Store back into dict
        self._nextExpTime[localType] = nextExpTime

        return exposure


    def init_roi(self, roiParam):
        minSizeX    = self._cshWinParamsX[0]

        if roiParam == ROI_ORIGIN_X:
            self._roiParamMin = 0
            self._roiParamMax = ((self._sensorSizeX - minSizeX) // self._clArgs.roiInc) * self._clArgs.roiInc
            roiStart = Roi.create( self._roiParamMin, 0, self._sensorSizeX, self._sensorSizeY)
            roiEnd   = Roi.create( self._roiParamMax, 0, self._sensorSizeX - self._roiParamMax, self._sensorSizeY)
        elif roiParam == ROI_ORIGIN_Y:
            self._roiParamMin = 0
            self._roiParamMax = ((self._sensorSizeY-1) // self._clArgs.roiInc) * self._clArgs.roiInc
            roiStart = Roi.create( 0, self._roiParamMin, self._sensorSizeX, self._sensorSizeY)
            roiEnd   = Roi.create( 0, self._roiParamMax, self._sensorSizeX, self._sensorSizeY - self._roiParamMax)
        elif roiParam == ROI_SIZE_X:
            self._roiParamMin = minSizeX
            self._roiParamMax = (self._sensorSizeX // self._clArgs.roiInc) * self._clArgs.roiInc
            roiStart = Roi.create( 0, 0, self._roiParamMin, self._sensorSizeY)
            roiEnd   = Roi.create( 0, 0, self._roiParamMax, self._sensorSizeY)
        else:  # roiParam == ROI_SIZE_Y:
            self._roiParamMin = 1
            self._roiParamMax = ((self._sensorSizeY) // self._clArgs.roiInc) * self._clArgs.roiInc
            roiStart = Roi.create( 0, 0, self._sensorSizeX, self._roiParamMin)
            roiEnd   = Roi.create( 0, 0, self._sensorSizeX, self._roiParamMax)

        roiInfo =  '  ROI {:5} Min/Max: {}/{}\n'.format(roiParam, self._roiParamMin, self._roiParamMax)
        roiInfo += '  Starting ROI origin: ({:>4},{:>4}), size=({:>4},{:>4})\n'.format(
            roiStart.origin().x(), roiStart.origin().y(), roiStart.size().x(), roiStart.size().y())
        roiInfo += '    Ending ROI origin: ({:>4},{:>4}), size=({:>4},{:>4})'.format(            
            roiEnd.origin().x(), roiEnd.origin().y(), roiEnd.size().x(), roiEnd.size().y())
        return roiInfo

    def create_roi(self, roiParam, paramValue):
        if roiParam == ROI_ORIGIN_X:
            roi = Roi.create( paramValue, 0, self._sensorSizeX - paramValue, self._sensorSizeY)
        elif roiParam == ROI_ORIGIN_Y:
            roi = Roi.create( 0, paramValue, self._sensorSizeX, self._sensorSizeY - paramValue)
        elif roiParam == ROI_SIZE_X:
            roi = Roi.create( 0, 0, paramValue, self._sensorSizeY)
        else:  # roiParam == ROI_SIZE_Y:
            roi = Roi.create( 0, 0, self._sensorSizeX, paramValue)
        return roi


    # Createas a new WinBin object for use in constructing a new camera c
    # configuration with the CamConfigFactory.
    #
    # hwROI - The hardware applied ROIs as a Roi object
    # hwBin - The hardware applied binning as a BinSize object
    def create_win_bin(self, hwRoi, hwBin=XYPair.create(1,1)):
        return WinBin.create( 
            WinBinHw.create( hwBin, hwRoi ),
            WinBinSw.create( Roi.create( 
                0, 0, 
                hwRoi.size().x() / hwBin.x(), 
                hwRoi.size().y() / hwBin.y() )))

    @ExecutionTime
    def bin(self,smode):
        global Bins
        global DEF_EXP_RATE, DEF_EXP_TIME
        global PROF_EXP_RATE, PROF_EXP_TIME
        success = True
        ccBaseName = 'cc_bin'
        ccDescription = 'Profile binning.'

        # Set Binning to something other than the first entry in the list so that the
        # first test has to actually do something.
        try:
            ccId = CamConfigId.create(ccBaseName)
            newCC = self._ccFactory.create(ccId, ccDescription, PROF_EXP_RATE, PROF_EXP_TIME) \
                                  .withWinBin( self.create_win_bin(self._roi_fullFrame, Bins[1]) )
        except DATComponentException as ex:
            print(ex.getMessage())
            return not success

        # Submit new CC to CSS and start it running so we'll be measuring the
        # time to stop, set features, and start on the first test config.
        if not self.submit_and_run_camConfig(newCC): return False

        self.reset_counters()
        self.show_setup(smode, None, None, self.bin_transitions_to_string(Bins))
        self.print_table_heading_row()      # Column labels for profiling data
        self.enable_timing_info_event()

        for bin in Bins:
            try:
                newCC = self._ccFactory.create(
                    ccId, ccDescription, False, Accum.create(), 
                    Exposure.create( PROF_EXP_RATE, PROF_EXP_TIME),
                    Normalization.create(), 
                    self.create_win_bin(self._roi_fullFrame, bin) )
            except DATComponentException as ex:
                print(ex.getMessage())
                success = False
                break

            self.ready_event_info_string(newCC)

            success = self.submit_camConfig(newCC)
            if not success: break

        print('')

        self.disable_timing_info_event()
        self.write_profile_data_to_file( self._current_profile_test_name + '_' + smode.lower() )

        return success


    @ExecutionTime
    def bin_exp(self,smode):
        global Bins
        global EXP_TIME_INC
        success = True
        ccBaseName = 'cc_bin_exp'
        ccDescription = 'Profile binning with changing exposure time.'

        self.init_exposure_times()         # Establish initial short/long exp times

        for type in self._clArgs.exposureTypes:
            self.init_exposure_times()

            # Set Binning and exposure time to something other than the first 
            # test so that the first test has to actually do something.
            # For exposure times, make sure initial exposure time is compatible
            # with the type
            try:
                ccId = CamConfigId.create(ccBaseName)
                newCC = self._ccFactory.create(
                    ccId, ccDescription, False, Accum.create(), 
                    Exposure.create(1.0 / (self._initExpTime[type] + 1.0), 
                                    self._initExpTime[type] * 2000.0),
                    Normalization.create(),
                    self.create_win_bin(self._roi_fullFrame, Bins[1]) )
            except DATComponentException as ex:
                print(ex.getMessage())
                success = False
                break

            # Submit new CC to CSS and start it running so we'll be measuring the
            # time to stop, set features, and start on the first test config.
            success = self.submit_and_run_camConfig(newCC)
            if not success: break

            self.reset_counters()
            self.show_setup(smode, type, None, self.bin_transitions_to_string(Bins))
            self.print_table_heading_row()      # Column labels for profiling data
            self.enable_timing_info_event()

            for bin in Bins:
                try:
                    newCC = self._ccFactory.create(
                        ccId, ccDescription, False, Accum.create(),
                        self.create_exposure(type, newCC.calculated().longExposureTransition()), 
                        Normalization.create(),
                        self.create_win_bin(self._roi_fullFrame, bin))
                except DATComponentException as ex:
                    print(ex.getMessage())
                    success = False
                    break

                self.ready_event_info_string(newCC)

                success = self.submit_camConfig(newCC)
                if not success: break

            if not success: break
       
            self.disable_timing_info_event()
            self.write_profile_data_to_file( 
                self._current_profile_test_name + '_' + smode.lower() + '_' + type)
            print('')

        return success


    @ExecutionTime
    def exp(self,smode):
        global EXP_RANGES
        global NUM, MIN, MAX, INC
        global DEF_EXP_RATE, DEF_EXP_TIME
        success = True
        ccBaseName = 'cc_exp'
        ccDescription = 'Profile exposure time.'

        try:
            ccId = CamConfigId.create(ccBaseName)

            # Create new CC so we can get at the calcualted min/max for the camera
            newCC = self._ccFactory.create(ccId, ccDescription, DEF_EXP_RATE, DEF_EXP_TIME)

            # Set min and max in ranges. 
            # NOTE: Minimum exposure time is shutter mode dependent
            # NOTE: Calculated minimum is in milliseconds
            EXP_RANGES[0][MIN] = newCC.calculated().minimumExposureTime() / 1000.0
            EXP_RANGES[len(EXP_RANGES)-1][MAX] = newCC.calculated().maximumExposureTime() / 1000.0

            # Format test info including camera min/max exposure time and test ranges
            expInfo = '        Exposure Time: min={:.06f}, max={:.06f}'.format(
                EXP_RANGES[0][MIN], EXP_RANGES[len(EXP_RANGES)-1][MAX])
            for r in EXP_RANGES:
                expInfo += '\n         Test Range {}: ({:9.06f} <= expTime < {:>11.06f}), increment: {:.06f}'.format(
                    r[NUM],r[MIN],r[MAX],r[INC])

            # Replace exposure info with somthing other than the first test value
            newCC = newCC.withExposure(
                Exposure.create(DEF_EXP_RATE, EXP_RANGES[0][MIN] * 2000.0))
        except DATComponentException as ex:
            print(ex.getMessage())
            return not success

        # Submit new CC to CSS and start it running so we'll be measuring the
        # time to stop, set features, and start on the first test config.
        if not self.submit_and_run_camConfig(newCC): return False

        self.reset_counters()
        self.show_setup(smode, None, None, expInfo)
        self.print_table_heading_row()   # Column labels for profiling data
        self.enable_timing_info_event()

        # while True:
        for r in EXP_RANGES:
            expTime = r[MIN]
            while expTime <= r[MAX]:
                # Set new exposure time(in ms) and a compatible rate in the camConfig
                try:
                    newCC = self._ccFactory.create(
                        ccId, ccDescription, False, Accum.create(), 
                        Exposure.create(1.0/(expTime+1.0), expTime * 1000.0),
                        Normalization.create(), 
                        self.create_win_bin(self._roi_fullFrame) )

                    self.ready_event_info_string(newCC)
                    success = self.submit_camConfig(newCC)
                    if not success: break

                    expTime = expTime + r[INC]    # Increment by defined exposure range increment 

                except DATComponentException as ex:
                    print(ex.getMessage())
                    success = False
                    break

            if not success: break

        print('')
        self.disable_timing_info_event()
        self.write_profile_data_to_file(self._current_profile_test_name + '_' + smode.lower())

        return success


    @ExecutionTime
    def roi(self,smode):
        ccId = None
        ccDescription = 'Profile ROI.'

        # Set Binning to something other than the first entry in the list so that the
        # first test has to actually do something.
        try:
            ccId = CamConfigId.create('cc_roi')
            newCC = self._ccFactory.create(ccId, ccDescription, DEF_EXP_RATE, DEF_EXP_TIME) 
        except DATComponentException as ex:
            print(ex.getMessage())
            return not success

        # Submit new CC to CSS and start it running so we'll be measuring the
        # time to stop, set features, and start on the first test config.
        if not self.submit_and_run_camConfig(newCC): return False

        for roiParam in self._clArgs.roiParams:

            roiSetup = self.init_roi(roiParam)           # Init min/max, print test ranges
            self.show_setup(smode, None, roiParam, roiSetup)
            self.reset_counters()
            self.print_table_heading_row()      # Column labels for profiling data
            self.enable_timing_info_event()

            success = True
            testValue = self._roiParamMin
            while testValue <= self._roiParamMax:
 
                try:
                    newCC = self._ccFactory.create(
                        ccId, ccDescription, False, Accum.create(), 
                        Exposure.create( DEF_EXP_RATE, DEF_EXP_TIME),
                        Normalization.create(), 
                        self.create_win_bin(self.create_roi(roiParam, testValue)) )
                except DATComponentException as ex:
                    print(ex.getMessage())
                    success = False
                    break

                self.ready_event_info_string(newCC)

                success = self.submit_camConfig(newCC)
                if not success: break

                testValue += self._clArgs.roiInc

            if not success: break

            self.disable_timing_info_event()
            self.write_profile_data_to_file( 
                self._current_profile_test_name + '_' + smode.lower() + '_' + roiParam)
            print('')

        return success


    @ExecutionTime
    def roi_exp(self,smode):
        ccId = CamConfigId.create('cc_roi')
        ccDescription = 'Profile ROI.'

        for expType in self._clArgs.exposureTypes:
            for roiParam in self._clArgs.roiParams:

                # self.init_exposure_times(refCC)    # Establish initial short/long exp times
                self.init_exposure_times()         # Establish initial short/long exp times
                roiSetup = self.init_roi(roiParam)           # Init min/max, print test ranges
                self.show_setup(smode, expType, roiParam, roiSetup)
                self.print_table_heading_row()      # Column labels for profiling data

                # Submit CC to CSS and start it running so we'll be measuring the
                # time to stop, set features, and start on the first test config.
                # For exposure times, make sure initial exposure time is compatible
                # with the type
                try:
                    newCC = self._ccFactory.create(
                        ccId, ccDescription, 
                        1.0 / (self._initExpTime[expType] + 1.0), 
                        self._initExpTime[expType] * 2000.0)
                except DATComponentException as ex:
                    print(ex.getMessage())
                    success = False
                    break

                # Submit new CC to CSS and start it running so we'll be measuring the
                # time to stop, set features, and start on the first test config.
                success = self.submit_and_run_camConfig(newCC)
                if not success: break

                self.reset_counters()
                self.enable_timing_info_event()

                success = True
                testValue = self._roiParamMin
                while testValue <= self._roiParamMax:
     
                    try:
                        newCC = self._ccFactory.create(
                            ccId, ccDescription, False, Accum.create(), 
                            self.create_exposure(
                                expType, newCC.calculated().longExposureTransition()), 
                            Normalization.create(), 
                            self.create_win_bin(
                                self.create_roi(roiParam, testValue)) )
                    except DATComponentException as ex:
                        print(ex.getMessage())
                        success = False
                        break

                    self.ready_event_info_string(newCC)

                    success = self.submit_camConfig(newCC)
                    if not success: break

                    testValue += self._clArgs.roiInc

                if not success: break

                self.disable_timing_info_event()
                self.write_profile_data_to_file( 
                    self._current_profile_test_name + '_' + smode.lower() + '_' + roiParam + '_' + expType)
                print('')

            if not success: break

        return success


    # Unsubscribes from timing info event and disables debug category 'PROFILE'
    def disable_timing_info_event(self):
        if not self._clArgs.dryRun:
          Event.unsubscribe( self._timingInfoEventName, self.listener)
          self._vcc.setDebugLevel('PROFILE', 0)     # Turn off Timing Event in CSS
        return


    # Enables debug category 'PROFILE', level 1 in controller and subscribes
    # to the timing info event
    def enable_timing_info_event(self):
        if not self._clArgs.dryRun: 
          self._vcc.setDebugLevel('PROFILE', 1)     # Turn on Timing Event in CSS
          Event.subscribe( self._timingInfoEventName, self.listener)
        return


    # Submits a camera configuration to the VCC. 
    # For the purposes of this profiling program, it is assumed that the
    # camOonfig ID has already been started in preview mode using
    # self.submit_and_run_camConfig(). Since the camConfig is already running,
    # any submit with a change to the running ID will cause the CSS to, if 
    # necessary, stop acquisition, apply the changes, and re0start that ID.
    #
    # camConfig - The camera configuration to submit to the CSS
    def submit_camConfig(self, camConfig):
        cfg = TestConfiguration()
        cfg.setFQPrefix(self._vccName)
        cfg.merge( camConfig.toAttributeTable().requalify(self._vccName) )
        cfg.requalify(self._vccName)
        return self.do_submitAndWait(cfg, 'submitting CamConfig')

    # Submits a configuration containing the specified camera configuraiton
    # and '.global:ID0' containing the camera configuration ID which will
    #
    # start the camConfig in preview mode.
    def submit_and_run_camConfig(self, camConfig):
        cfg = TestConfiguration()
        cfg.setFQPrefix(self._vccName)
        cfg.merge( camConfig.toAttributeTable().requalify(self._vccName) )
        cfg.insert('.global:ID0', camConfig.ID())
        cfg.requalify(self._vccName)
        return self.do_submitAndWait(cfg, 'submit and run CamConfig')


    # Submits a configuration to the VCC and waits for an action response.
    #
    # config - The config to be submitted
    # what - A short descriptive string of what is being submitted
    def do_submitAndWait(self, config, what):
        success = True

        if self._clArgs.verbose:
            print('Submitting configuration: {}\n'.format(config))

        if not self._clArgs.dryRun:
            ra = BaseMisc.submitAndWait(NullAction(), self._vcc, config)
            ar = ra.getActionResponse()
            if not ar == ActionResponse.OK:
                print('Failure {} - {}'.format(what, ar.errString))
                success = False
        return success


    # Deletes all camera configurations loaded during the course of 
    # execution of this program.
    def delete_all_ids(self):
        success = True
        msg = 'Deleting all loaded Execution Block IDs...'
        print(msg),
        cfg = TestConfiguration()
        cfg.setFQPrefix(self._vccName)
        cfg.insert('.global:deleteID', 'allExecBlocks')
        success = self.do_submitAndWait(cfg, msg)
        if success: print('OK!')

        msg = 'Deleting all loaded Camera Configuration IDs...'
        print(msg),
        cfg = TestConfiguration()
        cfg.setFQPrefix(self._vccName)
        cfg.insert('.global:deleteID', 'allCamConfigs')
        success = self.do_submitAndWait(cfg, msg)
        if success: print('OK!')
        return success


    # Executes the test specifed via the --profile command line switch.
    # 
    # name - The name of the test to execute
    def execute(self, name, smode):
        '''Dispatch method'''
        # Get the method from 'self'. Default to a lambda.
        self._current_profile_test_name = name
        method = getattr(self, name, lambda: 'Not Implemented')
        # Call the method as we return it
        return method(smode)

    # Prints the set of binning size transitions.
    #
    # bins - The set of binning sizes being profiled
    def bin_transitions_to_string(self, bins):
        binTransitions = '      Bin Transitions: '
        count = 0
        for bin in bins:
            binTransitions += '({},{}) ->'.format(bin.x(),bin.y())
            count = count + 1
            if count == 11: 
                binTransitions += '\n'
                binTransitions += ''.join([char*23 for char in ' '])
                count = 0
        binTransitions += 'Done'
        return binTransitions


    # Prints output table heading row
    def print_table_heading_row(self):
        global RESULTS_KEYS
        print('{:>5},{:>5},{:>5},{:^8},{:^8},{:^8},{:^8},{:^12},{:^9},{:^9},{:^9},{:^9},{:^9}, {}'.format(*RESULTS_KEYS))
        return


    # Formats a string containing the test number, current bin size, expsoure
    # time, and single ROI. The contents of this string will serve as a prefix
    # for the timing info event callback output.
    #
    # cc - Camera Configuration to obtain info from
    def ready_event_info_string(self, cc):
        global results
        global results_key_xref
        global dryRun_counter

        if self._clArgs.dryRun:
            dryRun_counter += 1
            results[results_key_xref[RESULTS_TEST]].append(dryRun_counter)
            results[results_key_xref[RESULTS_BINX]].append(cc.winBin().hw().binSize().x()) 
            results[results_key_xref[RESULTS_BINY]].append(cc.winBin().hw().binSize().y()) 
            results[results_key_xref[RESULTS_ORIGX]].append(cc.winBin().hw().ROI_1().origin().x()) 
            results[results_key_xref[RESULTS_ORIGY]].append(cc.winBin().hw().ROI_1().origin().y())
            results[results_key_xref[RESULTS_SIZEX]].append(cc.winBin().hw().ROI_1().size().x())
            results[results_key_xref[RESULTS_SIZEY]].append(cc.winBin().hw().ROI_1().size().y()) 
            results[results_key_xref[RESULTS_EXPTIME]].append(cc.exposure().time() / 1000.0)
            results[results_key_xref[RESULTS_STOP]].append(0.0)
            results[results_key_xref[RESULTS_CFGEXP]].append(0.0)
            results[results_key_xref[RESULTS_CFGWINBIN]].append(0.0)
            results[results_key_xref[RESULTS_START]].append(0.0)
            results[results_key_xref[RESULTS_TOTAL]].append(0.0)
            results[results_key_xref[RESULTS_CCID]].append(cc.ID())

            # Make a dict with the results key and the data just added and pass that 
            # to the print method for screen output
            currData = {key: results[results_key_xref[key]][len(results[results_key_xref[key]])-1] for key in RESULTS_KEYS}

            print_profile_data( currData )

        return


    def reset_counters(self):
        global event_counter
        global dryRun_counter
        event_counter = 0
        dryRun_counter = 0
        return


    # Restarts default camera configuration, as contained in the running instance
    # of the CSS. 
    # NOTE: This program does not modify 'cc_default'.
    def restart_cc_default(self):
        success = True
        cfg = TestConfiguration()
        cfg.setFQPrefix(self._vccName)
        cfg.insert('.global:ID0', 'cc_default')
        cfg.insert('.global:execCount0', 0)
        cfg.insert('.global:camMode','preview')
        cfg.insert('.global:triggerReference', 'absolute')

        msg = 'Restarting cc_default...'
        print(msg),
        success = self.do_submitAndWait(cfg, msg)
        if success: print('OK!')

        return success


    # Sets the ElectronicShutteringMode on the camera.
    #
    # mode - The shuttering mode to be set on the camera.
    def set_shutter_mode(self, mode):
        success = True
        # Submit new CC to CSS and start running it
        cfg = TestConfiguration()
        cfg.setFQPrefix(self._vccName)
        cfg.insert('.csh:shutter:mode', mode)
        cfg.requalify(self._vccName)
        success = self.do_submitAndWait(cfg, 'submitting shutter mode')
        if success: 
            self._ccFactory.setShutter( Shutter.create(Shutter.Mode.create(mode)) )

            # Now that the shutter mode is set, create a new reference camConfig. 
            # This will be used to get at the calculated values  which are 
            # dependent on the shutter mode
            try:
                self._refCC = self._ccFactory.create( 
                    CamConfigId.create('cc_ref'), 
                    'Reference CamConfig.', DEF_EXP_RATE, DEF_EXP_TIME)
            except DATComponentException as ex:
                print(ex.getMessage())
                return not success

            self._minExposureTime = self._refCC.calculated().minimumExposureTime() / 1000.0
            self._maxExposureTime = self._refCC.calculated().maximumExposureTime() / 1000.0

        return success


    # Prints generic information for the current profiling test.
    def show_setup(self, shutterMode, exposureType=None, roiParam=None, setup=None):
        print('Profiling Configuration: ')
        print('        CSS Mgmt Ctrl: {}'.format(self._vccName))
        print('        CSS Cam  Ctrl: {}'.format(self._vccCamName))
        print('              Profile: {}'.format(self._current_profile_test_name))
        print('         Shutter Mode: {}'.format(shutterMode))
        print('        Exposure Type: {}'.format(exposureType))
        print('            ROI Param: {}'.format(roiParam))
        print('{}'.format(setup))
        print('')


    # Performs any actions necessary when the test program is exiting.
    # cmdLineArgs - Comand Line arguments
    # success - Boolean flag indicating overall success status.
    def wrap_up(self,cmdLineArgs, success):
        print('\nWrapping up...')
        sleep(1)
        if cmdLineArgs.monitorOnly:
            who = "monitoring"
        else:
            who = "profiling"
            if cmdLineArgs.delete_all_ids:
                self.delete_all_ids()
            success = self.restart_cc_default()

        print('Wrap up complete, {} complete!'.format(who))

        if not cmdLineArgs.monitorOnly:
            print('Overall Status: '), 
            if success:
                print('SUCCESS')
            else:
                print('FAILURE')


    def write_profile_data_to_file(self, filename):
        global RESULTS_KEYS
        global results
        with open(filename+'.csv', 'w') as csvfile:
            sortedFields = sorted(results.keys())
            writer = csv.writer(csvfile)
            writer.writerow(RESULTS_KEYS)
            writer.writerows(zip(*[results[key] for key in sortedFields]))

        # clear results
        for key in results: results[key] = []


    @ExecutionTime
    def passively_monitor(self):
        global reset_counters

        print('Monitoring \'{}\' for timing info'.format(self._vccName))
        print('Press <return> to quit...')
        self.reset_counters()
        self.print_table_heading_row()
        self.enable_timing_info_event()

        # value = raw_input('Press any key to quit...\n')
        # print('Exiting...')
        sys.stdin = open('/dev/tty', 'r')
        raw_input()
        self.disable_timing_info_event()
        return

### Main program runtime ###

def main():
    print('Date: {}'.format(datetime.utcnow().strftime('%d/%m/%Y %H:%M:%S')))
    startTime = time.time()

    cmdLineArgs = cmdLineParser.parse_args()

    profile = Profile(cmdLineArgs)

    success = True
    if cmdLineArgs.delete_all_ids:
        success = profile.delete_all_ids()
 
    if success:
        # Monitor only is mutually exclusive with profiling proper.
        # If the flag is set, setup and monitor the specified vcc.
        # Monitoring terminates when the user presses the return key.
        if cmdLineArgs.monitorOnly:
            profile.passively_monitor()
        else:
            for what in cmdLineArgs.paramsToProfile:
                for smode in cmdLineArgs.shutterModes:
                    success = profile.set_shutter_mode(smode)
                    if not success: break

                    success = profile.execute(what, smode)

                    if not success: break
                    success = profile.restart_cc_default()
                    if not success: break

                if not success: break

    profile.wrap_up(cmdLineArgs, success)

if __name__ == '__main__':
    clear_execution_time_data()
    main()
    print_execution_time()
